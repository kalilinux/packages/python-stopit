Changes log
===========

1.1.0 - 2014-05-02
------------------

* Added support for TIMER signal based timeout control (Posix OS only)
* API changes due to new timeout controls
* An exhaustive documentation.

1.0.0 - 2014-02-09
------------------

Initial version
